import cv2
import time
import numpy as np


POSE_PAIRS = [[1, 0], [1, 2], [1, 5], [2, 3], [3, 4], [5, 6], [6, 7], [1, 8], [
    8, 9], [9, 10], [1, 11], [11, 12], [12, 13], [0, 14], [0, 15], [14, 16], [15, 17]]

keypointsMapping = ['Nose', 'Neck', 'R-Sho', 'R-Elb', 'R-Wr', 'L-Sho', 'L-Elb', 'L-Wr',
                    'R-Hip', 'R-Knee', 'R-Ank', 'L-Hip', 'L-Knee', 'L-Ank', 'R-Eye', 'L-Eye', 'R-Ear', 'L-Ear']

class PersonDetector:
    """
    This is the class to detect the person in an image with the pose coordiantes/keypoints
    """
    def __init__(self):
        self.protoFile = "../models/coco/pose_deploy_linevec.prototxt"    
        self.weightsFile = "../models/coco/pose_iter_440000.caffemodel"
        self.nPoints = 18
        #specifying input image dimensions
        self.inWidth = 368
        self.inHeight = 368
        self.threshold = 0.1



    def run(self,input_image):
        
        "Get the keypoints on an image by pasing a frame through the network"
        
        #read the image
        frame = cv2.imread(input_image)    
        frameCopy = np.copy(frame)
        frameWidth = frame.shape[1]
        frameHeight = frame.shape[0]
        
        #read the network into the memeory
        net = cv2.dnn.readNetFromCaffe(self.protoFile,self.weightsFile)

        t = time.time()
         # feeding frame to the network
        inpBlob = cv2.dnn.blobFromImage(frame, 1.0 / 255, (self.inWidth, self.inHeight),
                                        (0, 0, 0), swapRB=False, crop=False)
        # Set the prepared object as the input blob of the network
        net.setInput(inpBlob)
        #getting the inference/prediction from the forward method
        output = net.forward()
        print("time taken by network : {:.3f}".format(time.time() - t))

        H = output.shape[2]
        W = output.shape[3]

        # Empty list to store the detected keypoints
        points = []

        for i,keypoints in enumerate(keypointsMapping):
            # confidence map of corresponding body's part.
            probMap = output[0, i, :, :]

            # Find global maxima of the probMap.
            minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

            # Scale the point to fit on the original image
            x = (frameWidth * point[0]) / W
            y = (frameHeight * point[1]) / H

            if prob > self.threshold:
                cv2.circle(frameCopy, (int(x), int(y)), 3, (0, 255, 255),
                        thickness=-1, lineType=cv2.FILLED)
                cv2.putText(frameCopy, "{}".format(i), (int(x), int(
                     y)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2, lineType=cv2.LINE_AA)
                cv2.putText(frameCopy, "{}".format(keypoints), (int(x-10), int(
                    y+20)), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 255, 0), 1, lineType=cv2.LINE_AA)
                # Add the point to the list if the probability is greater than the threshold
                points.append((int(x), int(y)))
            else:
                points.append(None)

        # draw skeleton
        for pair in POSE_PAIRS:
            partA = pair[0]
            partB = pair[1]

            if points[partA] and points[partB]:
                cv2.line(frame, points[partA], points[partB], (0, 255, 255), 2)
                cv2.circle(frame, points[partA], 8, (0, 0, 255),
                        thickness=-1, lineType=cv2.FILLED)


        cv2.imshow('Output-Keypoints', frameCopy)
        cv2.imshow('Output-Skeleton', frame)

        cv2.imwrite('data/output_images/Output-Keypoints.jpg', frameCopy)
        cv2.imwrite('data/output_images/Output-Skeleton.jpg', frame)

        print("Total time taken : {:.3f}".format(time.time() - t))

        cv2.waitKey(0)


if __name__ == "__main__":

    input_source = "../data/input_images/virat.jpg"
    person = PersonDetector()
    person.run(input_source)
