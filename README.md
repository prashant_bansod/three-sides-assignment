threesides-assignment
==============================

This is a repository for the Three Sides Corp. assignment

Project Organization
------------


    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── input_images   <- The orignal input images
    │   └── output_images  <- The predictions from the model
    │ 
    ├── models             <- Trained and serialized models
    │    ├── coco
    │ 
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.generated with `pip freeze > requirements.txt`
    │ 
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   └── pose.py      <- main script for getting the pose estimation


How to run
------------

1. get the models from "<https://1drv.ms/u/s!As2kWF01vCbDgYATzHKjvkjkIevHsg?e=kgHY4n>" and put into the `models` folder
2. install the required enviroment packages `pip install -r requirements.txt`
3. run the `pose.py` script to get the pose estimation output
4. You can find the output images in the `data/output_images` folder

